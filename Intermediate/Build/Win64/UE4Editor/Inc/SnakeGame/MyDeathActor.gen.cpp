// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/MyDeathActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyDeathActor() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AMyDeathActor_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AMyDeathActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void AMyDeathActor::StaticRegisterNativesAMyDeathActor()
	{
	}
	UClass* Z_Construct_UClass_AMyDeathActor_NoRegister()
	{
		return AMyDeathActor::StaticClass();
	}
	struct Z_Construct_UClass_AMyDeathActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyRootComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyRootComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyDeathActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDeathActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MyDeathActor.h" },
		{ "ModuleRelativePath", "MyDeathActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDeathActor_Statics::NewProp_MyRootComponent_MetaData[] = {
		{ "Category", "MyDeathActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MyDeathActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyDeathActor_Statics::NewProp_MyRootComponent = { "MyRootComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyDeathActor, MyRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyDeathActor_Statics::NewProp_MyRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDeathActor_Statics::NewProp_MyRootComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyDeathActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyDeathActor_Statics::NewProp_MyRootComponent,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AMyDeathActor_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AMyDeathActor, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyDeathActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyDeathActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyDeathActor_Statics::ClassParams = {
		&AMyDeathActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMyDeathActor_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyDeathActor_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyDeathActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDeathActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyDeathActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyDeathActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyDeathActor, 4198906291);
	template<> SNAKEGAME_API UClass* StaticClass<AMyDeathActor>()
	{
		return AMyDeathActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyDeathActor(Z_Construct_UClass_AMyDeathActor, &AMyDeathActor::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AMyDeathActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyDeathActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
