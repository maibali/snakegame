// Fill out your copyright notice in the Description page of Project Settings.


#include "MyDeathActor.h"
#include "SnakeBase.h"

// Sets default values
AMyDeathActor::AMyDeathActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	
	class UStaticMesh* WallMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;


	MyRootComponent = CreateDefaultSubobject<USceneComponent>("RootModel");
	
	RootComponent = MyRootComponent;

	class UStaticMeshComponent* WallChank;
	WallChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall"));
	WallChank->SetStaticMesh(WallMesh);
	WallChank->SetRelativeLocation(FVector(0, 0, 0));
	
	WallChank->AttachTo(MyRootComponent);

}

// Called when the game starts or when spawned
void AMyDeathActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyDeathActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AMyDeathActor::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();

		}
	}
}


